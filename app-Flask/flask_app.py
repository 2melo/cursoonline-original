
# A very simple Flask Hello World app for you to get started with...

from flask import Flask, render_template
#from funcoes import *

app = Flask(__name__)


@app.route('/index')
def index():
    # ...
    # 'dnaoisndondaonsdiad
    # '
    # ...
    # hj, dia, mes, ano = hoje()

    # linha1 = " Hoje {} ".format(extenso(dia, mes, ano))

    return render_template('index.html')

@app.route('/cadastro')
def cadastro():
    return render_template("cadastro.html")

@app.route('/sobre')
def sobre():
    return render_template("sobre.html")

@app.route('/cursos')
def cursos():
    return render_template("cursos.html")

